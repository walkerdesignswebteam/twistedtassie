[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
    [[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
    [[!CommonTools? &cmd=`loadChunk` &name=`header`]]
    
    <div class="bgGallery" style="background-image: url([[*bannerImage:phpthumbof=`w=1800&h=1200&zc=1&q=40`]]);"></div>

    <section class="pagetitle pagetitleGallery image-bg overlay parallax">
        <div class="col-xs-12 text-center">
            <h1>[[*pagetitle]]</h1>
        </div>
    </section>

    <section class="gallery">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <ul id="filters" class="list-unstyled option-set clearfix" data-option-key="filter">
                        <li><a href="#filter" data-option-value="*" class="selected">View all</a></li>
                        <li><a href="#filter" data-option-value=".Images">Images</a></li>
                        <li><a href="#filter" data-option-value=".Videos">Videos</a></li>
                        <li><a href="#filter" data-option-value=".Slideshows">Slideshows</a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <ul id="newsListingContainer" class="list-unstyled">
                        [[*content]]
                    </ul>
                </div>
            </div>
        </div>
    </section>


    [[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

    <script src="template/js/jquery.isotope.min.js"></script>
    <script src="template/js/imagesloaded.pkgd.min.js"></script>
    <script src="template/js/flickity.pkgd.min.js"></script>
    <script src="template/js/magnific-1.0.0.js"></script>

    

    <script type="text/javascript">
    $(document).ready(function() {
        var $container = $('#newsListingContainer');
        $container.imagesLoaded(function() {
            $container.isotope({
                itemSelector : '.newsListingWrap',
                layoutMode : 'masonry'
            });
        });

        $('#filters a.selected').trigger("click");

        $('#filters a').click(function(e){
            e.preventDefault();

            var selector = $(this).attr('data-option-value');
            $('#newsListingContainer').isotope({ filter: selector });

            $(this).parents('ul').find('a').removeClass('selected');
            $(this).addClass('selected');
        });
    });
    </script>

    <script>
    $('.main-gallery').flickity({
        cellAlign: 'left',
        wrapAround: true,
        contain: true
    });
    </script>

    <script>
    $('.image-popup-vertical-fit').magnificPopup({ 
        type: 'image'
    });
    $('.popup-youtube').magnificPopup({ 
        type: 'iframe'
    });

    $('.popup-gallery').each(function() { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: 'a', // the selector for gallery item
            type: 'image',
            gallery: {
                enabled:true
            }
        });
    });


    </script>

</body>
</html>