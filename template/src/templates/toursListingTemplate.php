[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="pagetitle image-bg overlay parallax">
		<div class="background-image-holder">
			<img alt="Background Image" class="background-image" src="[[*bannerImage:phpthumbof=`w=1500&h=1000&zc=1&q=40`]]" />
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1>[[*pagetitle]]</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text-center">
					[[*content]]

					[[getResources? 
						&parents=`13` 
						&depth=`0` 
						&limit=`0`
						&tpl=`tourDetailsTpl` 
						&sortby=`menuindex`
						&sortdir=`ASC`
					]]
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`filming`]]

	<section class="imagesx3">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-4 noPadding">
					<img src="[[*imageLeft:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageMiddle:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageRight:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`bookNow`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`testimonials`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

</body>
</html>