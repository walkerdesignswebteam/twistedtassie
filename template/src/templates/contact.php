[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="pagetitle image-bg overlay parallax">
		<div class="background-image-holder">
			<img alt="Background Image" class="background-image" src="[[*bannerImage:phpthumbof=`w=1500&h=1000&zc=1&q=40`]]" />
		</div>
		<div class="col-xs-12 text-center">
			<h1>[[*pagetitle]]</h1>
		</div>
	</section>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					[[*content]]
				</div>
				<div class="col-sm-6">
					[[!CommonTools? &cmd=`loadChunk` &name=`ContactForm`]]
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`bookNow`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`testimonials`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

</body>
</html>