[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="pagetitle image-bg overlay parallax">
		<div class="background-image-holder">
			<img alt="Background Image" class="background-image" src="[[*bannerImage:phpthumbof=`w=1500&h=1000&zc=1&q=40`]]" />
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1>[[*pagetitle]]</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text-center">
					[[*content]]
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
					<ul id="testimonialWrap" class="list-unstyled">
						[[getImageList?
						&tvname=`testimonials`
						&tpl=`testimonialsListTpl`
						]]
					</ul>
			</div>
		</div>
	</section>

	<section class="imagesx3">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-4 noPadding">
					<img src="[[*imageLeft:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageMiddle:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageRight:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`bookNow`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

	<script src="template/js/masonry.pkgd.min.js"></script>
    <script src="template/js/imagesloaded.pkgd.min.js"></script>
    <script>

    var container = document.querySelector('#testimonialWrap');
    var msnry;
    imagesLoaded( container, function() {
        msnry = new Masonry( container, {
            itemSelector: '.testimonialList',
            animate: true
        }) 
    });
    </script>

</body>
</html>