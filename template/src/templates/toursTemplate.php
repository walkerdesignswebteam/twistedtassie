[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="pagetitle image-bg overlay parallax">
		<div class="background-image-holder">
			<img alt="Background Image" class="background-image" src="[[*bannerImage:phpthumbof=`w=1500&h=1000&zc=1&q=40`]]" />
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1>[[*pagetitle]]</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text-center">
					[[*content]]

					<a href="[[~39]]" class="btn btn-primary">Book this tour Now</a>

					<div class="main-gallery">
						[[getImageList? 
						&tvname=`gallerySlideshow` 
						&tpl=`gallerySlideshowTourTpl`]]
					</div>

					<h2>Other Tours</h2>

					[[getResources? 
						&parents=`13`
						&resources=`-[[*id]]`
						&depth=`0` 
						&limit=`0`
						&tpl=`tourDetailsSmallTpl` 
						&sortby=`menuindex`
						&sortdir=`ASC`
					]]
				</div>
			</div>
		</div>
	</section>

	<section class="imagesx3">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-4 noPadding">
					<img src="[[*imageLeft:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageMiddle:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
				<div class="col-xs-4 noPadding">
					<img src="[[*imageRight:phpthumbof=`w=700&h=500&zc=1&q=70&aoe=1`]]" alt="" class="img-responsive" />
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`testimonials`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

	<script src="template/js/imagesloaded.pkgd.min.js"></script>
    <script src="template/js/flickity.pkgd.min.js"></script>

	<script>
    $('.main-gallery').flickity({
        cellAlign: 'left',
        wrapAround: true,
        contain: true
    });
    </script>

</body>
</html>