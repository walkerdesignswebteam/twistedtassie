[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="pagetitle image-bg overlay parallax">
		<div class="background-image-holder">
			<img alt="Background Image" class="background-image" src="[[*bannerImage:phpthumbof=`w=1500&h=1000&zc=1&q=40`]]" />
		</div>
		<div class="col-xs-12 text-center">
			<h1>[[*pagetitle]]</h1>
		</div>
	</section>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text-center">
					[[*content]]
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
						[[getImageList?
						&tvname=`faqListing`
						&tpl=`faqListingTpl`
						]]
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="false">
						[[getImageList?
						&tvname=`faqListing2`
						&tpl=`faqListingTpl2`
						]]
					</div>
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`bookNow`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`testimonials`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

</body>
</html>