[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	<link rel="stylesheet" href="template/css/settings.css">
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<section class="slider">
		<div class="container-fluid">
			<div class="row">
				<div class="tp-banner-container">
					<div class="tp-banner">
						<ul>
							<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="client-assets/videos/twisted.jpg"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off"  data-title="Free on the Beach">
								<img src="client-assets/videos/twisted.jpg"  alt="Twisted Tassie video"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

								<div class="tp-caption tp-fade fadeout fullscreenvideo"
								data-x="0"
								data-y="0"
								data-speed="1000"
								data-start="1100"
								data-easing="Power4.easeOut"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1500"
								data-endeasing="Power4.easeIn"
								data-autoplay="true"
								data-autoplayonlyfirsttime="false"
								data-nextslideatend="true"
								data-volume="mute" data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on"			
								style="z-index: 2;"><video class="" preload="none" width="100%" height="100%" 
								poster='client-assets/videos/twisted.jpg'> 
								<source src='client-assets/videos/twisted.mp4' type='video/mp4' /></video></div>

								<div class="tp-caption sfb stt tp-resizeme"
								data-x="center" data-hoffset="0"
								data-y="center" data-voffset="-20"
								data-speed="600"
								data-start="2100"
								data-easing="Power4.easeOut"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								style="z-index: 2; white-space: nowrap;"><div class="sliderTextWrap"><div class="sliderHeading">TOUR SOME AMAZING ROADS<br/><span class="sliderText">Guided motorcycle tours of Tasmania</span></div></div></div>
							</li>

							<li data-transition="fade" data-slotamount="1" data-masterspeed="400"  data-saveperformance="off"  data-title="THE LAND OF 100,000 TURNS">

								<img src="/assets/components/phpthumbof/cache/queenstown.d06081e344b296946e6bb6ed7de1d186.jpg" class="sliderImage" alt="fullslide2" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" />

								<div class="tp-caption sfb stt tp-resizeme"
								data-x="center" data-hoffset="0"
								data-y="center" data-voffset="-150"
								data-speed="600"
								data-start="2100"
								data-easing="Power4.easeOut"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								style="z-index: 2; white-space: nowrap;"><div class="sliderTextWrap"><div class="sliderHeading">THE LAND OF 100,000 TURNS<br/><span class="sliderText">Motorcyclenews.com review</span></div></div></div>

								<div class="tp-caption sfb stt rs-parallaxlevel-0"
								data-x="center" data-hoffset="-300"
								data-y="center" data-voffset="170"
								data-speed="300"
								data-start="1450"
								data-easing="Power3.easeInOut"
								data-elementdelay="0.1"
								data-endelementdelay="0.1"
								data-endspeed="600"
								data-autoplay="false"
								data-autoplayonlyfirsttime="false"
								style="z-index: 11;"><iframe width="600" height="338" src="https://www.youtube.com/embed/nfc2qtOzeb0?rel=0" frameborder="0" allowfullscreen style="width:600px;height:338px;"></iframe></div>

						</li>

					<!-- 	[[getImageList?
						&tvname=`homeSlider`
						&tpl=`homeSliderTpl`
						]] -->
					</ul>
					<div class="tp-bannertimer"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="intro">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 text-center animated" data-animation="fadeIn" data-animation-delay="400">
				[[*content]]
				<img src="client-assets/Images/signature.png" class="img-responsive" alt="Twisted Tassie" />
			</div>
		</div>
	</div>
</section>

<section class="featuredVideo">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 animated" data-animation="fadeIn" data-animation-delay="500">
				[[*reviewText]]
				<a href="[[~31]]" class="btn btn-primary">See more Videos & Images</a>
			</div>
			<div class="col-sm-8 animated" data-animation="fadeIn" data-animation-delay="700">
				<iframe width="853" height="480" src="https://www.youtube.com/embed/nfc2qtOzeb0?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section>

<section class="tourDetails">
	<div class="container-fluid">
		<div class="row text-center">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="row">
					[[getResources? 
					&parents=`25` 
					&depth=`0` 
					&limit=`3`
					&tpl=`faqHomeTpl` 
					&sortby=`menuindex`
					&sortdir=`ASC`
					&includeTVs=`1`
					&includeTVList=`icon`
					]]
				</div>
				<a href="[[~25]]" class="btn btn-outline">See more FAQs</a>
			</div>
		</div>
	</div>
</section>


[[!CommonTools? &cmd=`loadChunk` &name=`bookNow`]]
[[!CommonTools? &cmd=`loadChunk` &name=`testimonials`]]
[[!CommonTools? &cmd=`loadChunk` &name=`filming`]]

[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

<script type="text/javascript" src="template/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="template/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript">

jQuery(document).ready(function() {

	jQuery('.tp-banner').show().revolution(
	{
		dottedOverlay:"none",
		delay:16000,
		startwidth:1700,
		startheight:700,
		hideThumbs:2000,

		thumbWidth:100,
		thumbHeight:50,
		thumbAmount:5,

		navigationType:"none",
		navigationArrows:"solo",
		navigationStyle:"preview4",

		touchenabled:"on",
		onHoverStop:"on",

		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,

		parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

		keyboardNavigation:"off",

		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:20,

		soloArrowLeftHalign:"left",
		soloArrowLeftValign:"center",
		soloArrowLeftHOffset:20,
		soloArrowLeftVOffset:0,

		soloArrowRightHalign:"right",
		soloArrowRightValign:"center",
		soloArrowRightHOffset:20,
		soloArrowRightVOffset:0,

		shadow:0,
		fullWidth:"off",
		fullScreen:"on",

		dottedOverlay:"twoxtwo",

		spinner:"spinner4",

		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,

		shuffle:"off",

		autoHeight:"off",                       
		forceFullWidth:"off",                       

		hideThumbsOnMobile:"off",
		hideNavDelayOnMobile:1500,                      
		hideBulletsOnMobile:"off",
		hideArrowsOnMobile:"off",
		hideThumbsUnderResolution:0,

		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0,
		fullScreenOffsetContainer: ""   
	});

	}); //ready

</script>
</body>
</html>