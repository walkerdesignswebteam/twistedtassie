[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	
	<div class="bgGallery" style="background-image: url([[*bannerImage:phpthumbof=`w=1800&h=1200&zc=1&q=40`]]);"></div>

	<section class="pagetitle image-bg overlay parallax">
		<div class="col-xs-12 text-center">
			<h1>[[*pagetitle]]</h1>
		</div>
	</section>

	<section class="intro">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					
					<p>Welcome to “Twisted Tassie” where the roads you will experience are virtually abandoned twisted bitumen paradise, this is motorcycle riders heaven. Of course I am biased as I have been riding these roads for nearly thirty years.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="featuredVideo">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h2>Featured Video</h2>
					<p>The Land of 100,000 Turns (Motorcyclenews.com) For four days, our road tester Michael Neeves got to ride Tassie's roads on a Ducati 1199 Panigale with a group of good mates on sportsbikes, and we all agreed that it was the only region in the world where you'll wear your tyres out on the edges before the middle.</p>
				</div>
				<div class="col-sm-8">
					<iframe width="853" height="480" src="https://www.youtube.com/embed/nfc2qtOzeb0?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>

	<section class="tourDetails">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h2>Featured Video</h2>
				</div>
			</div>
		</div>
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]

	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

</body>
</html>