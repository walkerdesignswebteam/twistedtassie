[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
		<div class="row">
			<main id="content" role="main" class="col-xs-8">
				<h1>[[*pagetitle]]</h1>
				<p class="post-info">
					<span class="left">Posted on [[*publishedon:strtotime:date=`%b %d, %Y`]] by <a href="[[~[[*parent]]]]author/[[*publishedby:userinfo=`username`]]">[[*publishedby:userinfo=`username`]]</a></span>
					[[*articlestags:notempty=`<span class="tags left">&nbsp;| Tags: [[+article_tags]]</span>`]]
				</p>
				[[*content]]
			</main>
			<aside class="col-xs-4">
				Sidebar
				[[!CommonTools? &cmd=`loadChunk` &name=`articleSidebar`]] 
			</aside>
		</div><!--/.row -->
		[[!CommonTools? &cmd=`loadChunk` &name=`footer`]] 
	</div><!-- /container -->
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>