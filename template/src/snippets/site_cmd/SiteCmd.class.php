<?php

//may want to require this is using formitbuilder forms (perhaps load as needed rather than in class head.
//require_once MODX_CORE_PATH.'components/formitbuilder/model/formitbuilder/FormItBuilder.class.php';
//Note - You can of course create a global reference to this object in order to run directly via PHP.
require_once 'Core.class.php';

class SiteCmd extends Core {

    function __construct(&$modx) {
        $this->modx = &$modx;
        parent::__construct($this->modx);
    }

    //[[!site_cmd? &json_options=`{"cmd":"output_mycontent"}`]]
    function requestOutput($json_options, $extraArg = NULL) {
        $s_ret = '';
        $o_commands = json_decode($json_options);
        switch ($o_commands->cmd) {
            case 'output_mycontent':
                $s_ret .= $this->output_mycontent();
                break;
            case 'outputForm_contactForm':
                $s_ret .= $this->outputForm_contactForm();
                break;
            case 'output_searchResults': $s_ret .= $this->output_searchResults();
                break;
            case 'output_SEOSummary': 
                $s_ret .= $this->output_SEOSummary();
                break;
            case 'output_timeSince': 
                $s_ret .= $this->output_timeSince($o_commands->time);
                break;
            default: return '--- ' . $o_commands->cmd . ' is not a valid command ---';
        }
        return $s_ret;
    }

    private function output_timeSince($time){
        $o_now = new DateTime();
        $o_then = new DateTime($time);
        $o_diff = $o_now->diff($o_then);
        $s_ret = '';
        $days = $o_diff->format('%d');
        $months = $o_diff->format('%m');
        if(empty($days)){
            $s_ret .= 'Today';
        }else if(!empty($months)){
            if($months == 1){
                $s_ret .= $o_diff->format('%m month ago'); //$months.' month ago'; 
            }else{
                $s_ret .= $o_diff->format('%m months ago'); //$months.' months ago';
            }
        }else{
            if($days == 1){
                $s_ret .= 'Yesterday';
            }else{
                $s_ret .= $o_diff->format('%d days ago'); //$days.' days ago';
            }
        }
        return $s_ret;
    }
    
    public function output_searchResults() {
        $s_searchVal = $this->getVal('search');
        $s_ret = '<div class="siteSearchForm">[[!SimpleSearchForm]]</div>';

        if (empty($s_searchVal) === false && strlen($s_searchVal) > 2) {

            $s_ret.= '<hr />

                    [[!SimpleSearch?
                        &toPlaceholder=`sisea.results`
                        &perPage=`500`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]]`
                        &operator=`EQ`
                        &operand=`0`
                        &then=``
                        &else=`<h2>Found Pages ([[+sisea.total]])</h2>[[+sisea.results]]`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]][[+sisea.places.total]]`
                        &operator=`EQ`
                        &operand=`00`
                        &then=`<h2>No results found</h2>`
                    ]]
            ';
        }
        return $s_ret;
    }

    function output_mycontent() {
        return 'blah';
    }

    function outputForm_contactForm() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_name = new JsonFormBuilder_elementText('name_full', 'Your Name');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_notes = new JsonFormBuilder_elementTextArea('comments', 'Comments', 5, 30);

        $a_opts = array(
            '' => 'Please select...',
            'Yes' => 'Yes',
            'No' => 'No',
        );
        $o_fe_havedog = new JsonFormBuilder_elementSelect('havedog', 'Do you have a dog?', $a_opts);
        $o_fe_dogname = new JsonFormBuilder_elementText('dogname', 'Name of Dog');

        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Submit Form', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array($o_fe_havedog, $o_fe_notes, $o_fe_name, $o_fe_email);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
        }


        //NOTE: Requires jQuery Validate to work.
        //Conditional required rule example
        $a_formRules[] = new FormRule(FormRuleType::required, $o_fe_dogname, NULL, 'As you have a dog, please tell us its name.', array($o_fe_havedog, 'Yes'));
        //You can create a Show rule which will keep the field hidden, unless the value of another field is selected.
        $a_formRules[] = new FormRule(FormRuleType::conditionShow, $o_fe_dogname, NULL, NULL, array($o_fe_havedog, 'Yes'));



        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');

        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'contactForm');
        $o_form->setRedirectDocument(3);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('JsonFormBuilder Contact Form Submission - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the contact us form:</p>');
        $o_form->setJqueryValidation(true);
        $o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    $o_fe_name, $o_fe_email, $o_fe_notes, $o_fe_havedog, $o_fe_dogname, $o_fe_buttSubmit
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }

    private function output_SEOSummary(){
        require_once MODX_BASE_PATH.'template/src/snippets/SEOSummary.class.php';
        $o_seoSummary = new SEOSummary($this->modx);
        $o_seoSummary->outputSummary();
    }
    
}
