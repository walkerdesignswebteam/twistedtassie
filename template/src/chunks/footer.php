<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<img src="template/images/twisted-tassie-logo-footer.png" alt="Twisted Tassie Logo" class="img-responsive" />
				<ul class="list-unstyled socialIcons">
					<li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube fa-lg"></i></a></li>
				</ul>
				<p>&copy; 2015 [[++site_name]]. All rights reserved.</p>
				<p>Website by <a href="http://walkerdesigns.com.au/web/" rel="external" title="Website design, development, hosting and copywriting by Walker Designs, Launceston, Hobart, Tasmania, Australia">Walker Designs</a>
				</p>
			</div>
		</div>
	</div>
</footer>