[[!JsonFormBuilder-fromJSON? &json=`
{
"id": "ContactForm",
"redirectDocument": "8",
"jqueryValidation":true,
"placeholderJavascript":"JsonFormBuilder_ContactForm",

"emailToAddress": "glen@walkerdesigns.com.au",
"emailFromAddress": "[[+postVal.email_address]]",
"emailFromName": "[[++site_name]] website",
"emailSubject": "Booking form submission",
"emailHeadHtml": "<p>This is a submission from the [[++site_name]] website:</p>",

"elements": [
"<fieldset>",
{"element":"hidden", "id":"user_group", "label":"User Group", "defaultVal":3},

{"element":"text", "id":"name_full", "label":"Your Name", "extraClasses":["full"],"rules":["required"] },
{"element":"text", "id":"phone", "label":"Phone", "extraClasses":["full"],"rules":["required"] },
{"element":"text", "id":"email_address", "label":"Email Address", "extraClasses":["full"], "rules":["required","email"] },
{"element":"text", "id":"startDate", "label":"Start date", "extraClasses":["half"],"rules":["required"] },
{"element":"text", "id":"endDate", "label":"End date", "extraClasses":["half"],"rules":["required"] },

{"element":"textArea", "id":"details", "label":"Details", "rows":5,"columns":30},

{"element":"button", "id":"submit", "label":"Request a Booking", "type":"Submit"},

"</fieldset>"
]
}
`]]

<script type="text/javascript">
// <![CDATA[
[[+JsonFormBuilder_ContactForm]]
// ]]>
</script> 