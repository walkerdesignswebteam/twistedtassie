
<header class="header">
	<div class="container-fluid">
		<div class="row">
			<div class="logoWrap">
				<div class="logo">
					<a href="[[~1]]" title="Twisted Tassie"><img src="template/images/twisted-tassie-logo.png" alt="Twisted Tassie Motorcycle Tours" /></a>
				</div>
			</div>
			<div id="navDrop">

				<div class="btn-menu-wrapper">
					<a class="homeLink" href="[[~1]]">HOME</a>
					<a id="btn-menu" href="#">
						<span class="label">Menu</span>

						<span class="icon-container">
							<span class="line line01"></span>
							<span class="line line02"></span>
							<span class="line line03"></span>
							<span class="line line04"></span>
						</span>
					</a>
				</div>

				<div class="navDrop">
						<ul>
							[[getResources? 
							&parents=`0` 
							&resources=`-1`
							&depth=`0` 
							&limit=`6`
							&tpl=`navMainTpl` 
							&sortby=`menuindex`
							&sortdir=`ASC`
							&includeTVs=`1`
							&includeTVList=`icon`
							]]
						</ul>
				</div>
			</div>
			
		</div>
	</div>
</header>