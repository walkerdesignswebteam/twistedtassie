<section class="filming">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-1">
				<img src="client-assets/Images/filming.jpg" alt="motorcycle filming" class="img-responsive" />
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="animated" data-animation="fadeIn" data-animation-delay="700">
					[[getResourceField? &id=`1` &field=`filmingText` &processTV=`1`]]
				</div>
			</div>
		</div>
	</div>
</section>



