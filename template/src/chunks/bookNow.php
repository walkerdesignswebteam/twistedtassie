<section class="bookNow">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 noPadding">
				<div class="bookNowImage animated" data-animation="fadeIn" data-animation-delay="500" style="background-image: url([[*bookNowImage:phpthumbof=`w=900&h=530&zc=1&q=40`]]);"></div>
			</div>
			<div class="col-sm-6">
				<div class="bookNowText animated" data-animation="fadeIn" data-animation-delay="700">
					[[getResourceField? &id=`1` &field=`bookNowText` &processTV=`1`]]
					<a href="[[~13]]" class="btn btn-primary">Find out more</a>
				</div>
			</div>
		</div>
	</div>
</section>