	<section class="testimonialsSection" style="background-image: url([[*testimonialsImage:phpthumbof=`w=1800&h=580&zc=1&q=42`]]);">
		<div class="testimonialsOverlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="testimonials animated" data-animation="fadeIn" data-animation-delay="500">
						<ul class="list-unstyled">
							[[!getImageList?
							&docid=`24`
							&tvname=`testimonials`
							&tpl=`testimonialsTpl`
							&limit=`1`
							&randomize=`1`
							]]
						</ul>
						<a href="[[~24]]" class="btn btn-outline">Read More Testimonials</a>
					</div>
				</div>
			</div>
		</div>
	</section>