function highlightActiveLinks(a_menuPrefixArray,parentIdString){
	//e.g. highlightActiveLinks(['mm_','mobmm_'],'[[*id]],[[CommonTools? &cmd=`parentIds` &pageID=`[[*id]]`]]');
	var a_ids=parentIdString.split(',');
	var i=0;
	var j=0;
	for(i=0;i<a_menuPrefixArray.length;i++){
		for(j=0;j<a_ids.length;j++){
			var listEl = $('#'+a_menuPrefixArray[i]+a_ids[j]);
			if(listEl.length>0){
				listEl.addClass('active');
			}
		}
	}
}
    // --------------------------------------------
    // Platform detect
    // --------------------------------------------
     var mobileTest;
     if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        mobileTest = true;
        $("html").addClass("mobile");
    }
    else {
        mobileTest = false;
        $("html").addClass("no-mobile");
    }
    
    var mozillaTest;
    if (/mozilla/.test(navigator.userAgent)) {
        mozillaTest = true;
    }
    else {
        mozillaTest = false;
    }
    var safariTest;
    if (/safari/.test(navigator.userAgent)) {
        safariTest = true;
    }
    else {
        safariTest = false;
    }
    
    // Detect touch devices    
    if (!("ontouchstart" in document.documentElement)) {
        document.documentElement.className += " no-touch";
    }
    

// -------------------------------------------
    // Parallax
    // -------------------------------------------

    $(window).bind('load', function () {
        init_parallax();                       
    });


    function init_parallax(){

        // Parallax        
        if (($(window).width() >= 1024) && (mobileTest == false)) {
            $(".parallax-1").parallax("50%", 0.1);
            $(".parallax-2").parallax("50%", 0.2);
            $(".parallax-3").parallax("50%", 0.3);
            $(".parallax-4").parallax("50%", 0.4);
            $(".parallax-5").parallax("50%", 0.5);
        }
    }


    

// Makes ALL vimeo + youtube videos responsive without anything fancy.
(function ( window, document, undefined ) {

    /*
    * Grab all iframes on the page or return
    */
    var iframes = document.getElementsByTagName( 'iframe' );

    /*
    * Loop through the iframes array
    */
    for ( var i = 0; i < iframes.length; i++ ) {

        var iframe = iframes[i],

        /*
        * RegExp, extend this if you need more players
        */
        players = /www.youtube.com|player.vimeo.com/;

        /*
        * If the RegExp pattern exists within the current iframe
        */
        if ( iframe.src.search( players ) > 0 ) {

            /*
            * Calculate the video ratio based on the iframe's w/h dimensions
            */
            var videoRatio        = ( iframe.height / iframe.width ) * 100;
            
            /*
            * Replace the iframe's dimensions and position
            * the iframe absolute, this is the trick to emulate
            * the video ratio
            */
            iframe.style.position = 'absolute';
            iframe.style.top      = '0';
            iframe.style.left     = '0';
            iframe.width          = '100%';
            iframe.height         = '100%';
            
            /*
            * Wrap the iframe in a new <div> which uses a
            * dynamically fetched padding-top property based
            * on the video's w/h dimensions
            */
            var wrap              = document.createElement( 'div' );
            wrap.className        = 'fluid-vids';
            wrap.style.width      = '100%';
            wrap.style.position   = 'relative';
            wrap.style.paddingTop = videoRatio + '%';
            
            /*
            * Add the iframe inside our newly created <div>
            */
            var iframeParent      = iframe.parentNode;
            iframeParent.insertBefore( wrap, iframe );
            wrap.appendChild( iframe );

        }

    }

})( window, document );

jQuery(document).ready(function(){

    $(".placeholderField").focus(function(){
    	if($(this).val()==$(this).attr('title')){
        	$(this).val('');
        }
    });
    $(".placeholderField").blur(function(){
    	if($(this).val()==''){
       		$(this).val($(this).attr('title'));
       	}
    });
    $(".placeholderField").blur();

    /*********************************************************************************************/
    /*External links to _blank target (allows valid XHTML strict whilst opening external windows)*/
    /*********************************************************************************************/
    $('a[rel="external"]').click(function(){
	    this.target = "_blank";
    });
	
    /*************************/
    /* Window resize scripts */
    /*************************/
    function resizeSets(){

    }
    $(window).resize(function(e){resizeSets();});
    resizeSets();



    // Append .background-image-holder <img>'s as CSS backgrounds

    $('.background-image-holder').each(function() {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });

    // Fade in background images

    setTimeout(function() {
        $('.background-image-holder').each(function() {
            $(this).addClass('fadeIn');
        });
    }, 200);


    //----------------------------------------------------------------------------/
    // Animated Items
    //----------------------------------------------------------------------------/
    
    'use strict';

    $('.animated').appear(function() {
        var elem = $(this);
        var animation = elem.data('animation');
        if ( !elem.hasClass('visible') ) {
            var animationDelay = elem.data('animation-delay');
            if ( animationDelay ) {

                setTimeout(function(){
                    elem.addClass( animation + " visible" );
                }, animationDelay);

            } else {
                elem.addClass( animation + " visible" );
            }
        }
    });

    setTimeout(function(){
        $('body').addClass('loaded');
    }, 3000);


    //----------------------------------------------------------------------------/
    // Hamburger menu
    //----------------------------------------------------------------

    $('#btn-menu').attr('href','javascript:;');
    $( "#btn-menu" ).click(function() {
        $("#navDrop").toggleClass('navDropOpen');
        $(".logoWrap").toggleClass('navDropLogoOpen');
    });


    //----------------------------------------------------------------------------/
    // Header on scroll
    //----------------------------------------------------------------

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            $(".header").addClass("darkHeader");
        } else {
            $(".header").removeClass("darkHeader");
        }
    });
	
});

